CREATE TABLE Products
(
	id int NOT NULL IDENTITY(1,1),
	name varchar(100),

	PRIMARY KEY (id)
);

CREATE TABLE Categories
(
	id int NOT NULL IDENTITY(1,1),
	name varchar(100),

	PRIMARY KEY (id)
);

CREATE TABLE ProductCategories
(
	id int NOT NULL IDENTITY(1,1),
	product_id int,
	category_id int,

	PRIMARY KEY (id),
	FOREIGN KEY (product_id) REFERENCES Products(id),
	FOREIGN KEY (category_id) REFERENCES Categories(id),
);


-- adding some data

--INSERT INTO Products (name) 
--VALUES
--	('Milk'),
--	('Bread'),
--	('Salt');
	
--INSERT INTO Categories (name) 
--VALUES
--	('Milk Products'),
--	('For breakfast'),
--	('Seasoning'),
--	('Meat');
	
--INSERT INTO ProductCategories (product_id, category_id) 
--VALUES
--	(1, 1),
--	(1, 2),
--	(3, 3);
	
	
SELECT Products.Name AS Product, Categories.Name AS Category
FROM Products
LEFT JOIN ProductCategories ON Products.id = ProductCategories.product_id
LEFT JOIN Categories ON Categories.id = ProductCategories.category_id;