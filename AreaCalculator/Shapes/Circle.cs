﻿namespace Shapes
{
    public class Circle : IShape
    {
        private readonly double _radius;


        public Circle(double radius)
        {
            if (!IsRadiusValid(radius))
            {
                throw new ArgumentException($"Invalid radius value ({ radius }).");
            }

            _radius = radius;
        }

        public double GetArea()
        {
            return Math.PI * _radius * _radius;
        }


        private static bool IsRadiusValid(double radius)
        {
            return radius >= 0;
        }
    }
}
