﻿namespace Shapes
{
    public class Triangle : IShape
    {
        private readonly double _maxSide;
        private readonly double[] _otherSides;


        public Triangle(double side1, double side2, double side3)
        {
            if (!IsSideValid(side1) || !IsSideValid(side2) || !IsSideValid(side3))
            {
                throw new ArgumentException("Invalid one or more side values.");
            }

            List<double> sides = new List<double> { side1, side2, side3 };

            double maxSide = sides.Max();
            sides.Remove(maxSide);
            double[] otherSides = sides.ToArray();

            if (!IsPossible(maxSide, otherSides))
            {
                throw new ArgumentException(
                    "Impossible to build triangle with these sides:" +
                    $"({ side1 }, { side2 }, { side3 }).");
            }

            _maxSide = maxSide;
            _otherSides = otherSides;
        }


        public double GetArea()
        {
            if (IsRightAngled())
            {
                // base * height / 2
                return _otherSides[0] * _otherSides[1] / 2;
            }
            else
            {
                // Heron's formula
                double semiPerimeter = (_maxSide + _otherSides.Sum()) / 2;
                double area = Math.Sqrt(semiPerimeter
                    * (semiPerimeter - _otherSides[0])
                    * (semiPerimeter - _otherSides[1])
                    * (semiPerimeter - _maxSide));

                return area;
            }
        }


        private bool IsRightAngled()
        {
            const double precision = 1E-12;

            // Is not degenerate triangle and right angled (via Pythagorean theorem)
            return
                _maxSide > 0 && _otherSides.All(x => x > 0) &&
                Math.Abs((_maxSide * _maxSide) - _otherSides.Sum(x => x * x)) <= precision;
        }


        private static bool IsSideValid(double side)
        {
            return side >= 0;
        }

        private static bool IsPossible(double maxSide, double[] otherSides)
        {
            return maxSide <= otherSides.Sum();
        }
    }
}
