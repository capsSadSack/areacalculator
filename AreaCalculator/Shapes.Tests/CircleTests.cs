﻿namespace Shapes.Tests
{
    internal class CircleTests
    {
        [Test]
        public void InvalidRadius_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                Circle circle = new Circle(-1);
            });
        }

        [TestCase(0, 0, TestName ="Degenerate circle - Zero area")]
        [TestCase(2, 12.566370614359, TestName = "Integer radius")]
        [TestCase(3.1, 30.190705400998, TestName = "Double radius")]
        public void GetArea_CorrectResult(double radius, double expectedArea)
        {
            Circle circle = new Circle(radius);
            double actualArea = circle.GetArea();

            Assert.That(actualArea, Is.EqualTo(expectedArea).Within(Consts.PRECISION));
        }
    }
}
