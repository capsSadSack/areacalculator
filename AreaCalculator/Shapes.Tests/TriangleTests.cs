namespace Shapes.Tests
{
    public class TriangleTests
    {
        [TestCase(-1, 1, 1, TestName = "Negative side")]
        [TestCase(1, 2, 10, TestName = "Impossible triangle")]
        public void InvalidSides_ArgumentException(double side1, double side2, double side3)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                Triangle triangle = new Triangle(side1, side2, side3);
            });
        }

        [TestCase(0, 1, 1, TestName = "One zero side with equal others")]
        [TestCase(1, 2, 3, TestName = "All sides on one line")]
        public void DegenerateTriangle_NoException(double side1, double side2, double side3)
        {
            Triangle triangle = new Triangle(side1, side2, side3);
        }

        [TestCase(3, 4, 5, 6, TestName = "GetArea - Right angle")]
        [TestCase(5, 7, 10, 16.248076809272, TestName = "GetArea - One obtuse angle")]
        [TestCase(3, 5, 6, 7.483314773548, TestName = "GetArea - One acute angle")]
        [TestCase(0, 1, 1, 0, TestName = "GetArea - Degenerate triangle with zero side")]
        [TestCase(1, 2, 3, 0, TestName = "GetArea - Degenerate triangle with non-zero sides")]
        public void GetArea_CorrectResult(double side1, double side2, double side3, 
            double expectedArea)
        {
            Triangle triangle = new Triangle(side1, side2, side3);
            double actualArea = triangle.GetArea();

            Assert.That(actualArea, Is.EqualTo(expectedArea).Within(Consts.PRECISION));
        }
    }
}